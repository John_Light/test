<?php
namespace Test\RequestPrice\Block\Adminhtml\EditButtons;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Save Request'),
            'class' => 'save primary',
            'on_click' => sprintf("location.href = '%s';", $this->getUrl('*/*/save')),
            'sort_order' => 90,
        ];
    }
}