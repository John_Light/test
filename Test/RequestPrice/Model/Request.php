<?php
namespace Test\RequestPrice\Model;

use Magento\Framework\Model\AbstractModel;

class Request extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Test\RequestPrice\Model\ResourceModel\Request');
    }
}

