<?php
namespace Test\RequestPrice\Model\DataProvider;

use Test\RequestPrice\Model\ResourceModel\Request\CollectionFactory;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;

class Form extends \Magento\Ui\DataProvider\AbstractDataProvider implements DataProviderInterface
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $requestCollection,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $requestCollection->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        $this->loadedData = array();

        foreach ($items as $request) {
            $this->loadedData[$request->getId()]['requestprice_edit'] = $request->getData();
        }

        return $this->loadedData;
    }
}