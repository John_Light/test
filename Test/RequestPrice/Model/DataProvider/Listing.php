<?php
namespace Test\RequestPrice\Model\DataProvider;

use Test\RequestPrice\Model\ResourceModel\Request\CollectionFactory as RequestFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;

class Listing extends AbstractDataProvider implements DataProviderInterface
{
    protected $collection;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        RequestFactory $collection,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collection->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        return $items;
    }
}