<?php
namespace Test\RequestPrice\Model\ResourceModel;

class Request extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('test_request_price', 'request_id');
    }
}

