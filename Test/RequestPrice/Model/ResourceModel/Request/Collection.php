<?php
namespace Test\RequestPrice\Model\ResourceModel\Request;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Test\RequestPrice\Model\Request',
            'Test\RequestPrice\Model\ResourceModel\Request'
        );

    }
}