<?php
namespace Test\RequestPrice\Controller\Index;
use \Magento\Framework\Exception\NotFoundException;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_requestModel;
    protected $_jsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Test\RequestPrice\Model\RequestFactory $requestFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    )
    {
        $this->_requestModel = $requestFactory;
        $this->_jsonFactory = $jsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->getRequest();
        if($request->isAjax()) {
            $model = $this->_requestModel->create();

            $data = $request->getPostValue();
            foreach($data as $k=>$v){
                $data[$k] = htmlspecialchars($v);
            }

            $data['date_created'] = date('Y-m-d');
            $data['status'] = 0;

            $model->setData($data);
            $model->save();

            return  $this->_jsonFactory->create()->setData(['message' => 'Success']);
        } else {
            throw new NotFoundException(__('Not ajax'));
        }
    }
}