<?php
namespace Test\RequestPrice\Controller\Adminhtml\Request;

class Add extends \Magento\Backend\App\Action
{
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Test_RequestPrice::test_requestprice');
    }

    public function execute()
    {
        $this->_forward('edit');
    }
}