<?php
namespace Test\RequestPrice\Controller\Adminhtml\Request;

//use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;

class Edit extends Action
{
    const RESOURCE = 'Test_RequestPrice::test_requestprice';
    const MENU = 'Test_RequestPrice::test_requestprice';
    const TITLE = 'Request Edit';

    protected $_coreRegistry = null;
    protected $resultPageFactory;
    protected $_request;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Test\RequestPrice\Model\RequestFactory $requestFactory,
        \Magento\Backend\Model\SessionFactory $sessionFactory
    )
    {
        $this->session = $sessionFactory;
        $this->_requestFactory = $requestFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::RESOURCE);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu(self::TITLE);

        return $resultPage;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('request_id');

        $model = $this->_requestFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $resultRedirect = $this->resultRedirectFactory->create(ResultFactory::TYPE_PAGE);

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->session->create()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

//        $this->_coreRegistry->register('request_id', $model);

        $resultPage = $this->_initAction();

        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Request'));

        return $resultPage;
    }
}