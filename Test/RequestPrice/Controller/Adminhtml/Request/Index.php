<?php
namespace Test\RequestPrice\Controller\Adminhtml\Request;

use Magento\Backend\App\Action as backection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;

class Index extends Action
{
    const RESOURCE = 'Test_RequestPrice::test_requestprice';
    const MENU = 'Test_RequestPrice::test_requestprice';
    const TITLE = 'Price Requests Grid';

    protected function _isAllowed()
    {
        $result = parent::_isAllowed();
        $result = $result && $this->_authorization->isAllowed(self::RESOURCE);

        return $result;
    }

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu(self::MENU);
        $resultPage->getConfig()->getTitle()->prepend(__(self::TITLE));

        return $resultPage;
    }
}