<?php
namespace Test\RequestPrice\Controller\Adminhtml\Request;

//use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;

class Save extends Action
{
    const RESOURCE = 'Test_RequestPrice::test_requestprice';

    protected $_coreRegistry = null;
    protected $resultPageFactory;
    protected $_request;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Test\RequestPrice\Model\RequestFactory $requestFactory,
        \Magento\Backend\Model\SessionFactory $sessionFactory
    )
    {
        $this->session = $sessionFactory;
        $this->_requestFactory = $requestFactory;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::RESOURCE);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        $data = $post['requestprice_edit'];

        $resultRedirect = $this->resultRedirectFactory->create();
        if(is_array($data)){
            $requestModel = $this->_requestFactory->create();

            if(isset($data['request_id'])){
                $requestModel->load($data['request_id']);
            }

            $requestModel->setData($data);
            try{
                $requestModel->save();
                $this->messageManager->addSuccess(__('Request Saved'));
                $session = $this->session->create();
                $session->setFormData(false);
            } catch(\Exception $e){
                $this->messageManager->addError($e, __('Something went wrong while saving request'));
                $session->setFormData($data);
                if(isset($data['request_id'])){
                    return $resultRedirect->setPath('*/*/edit', ['request_id' => $data['request_id']]);
                } else{
                    return $resultRedirect->setPath('*/*/edit');
                }
            }
        }

        return $resultRedirect->setPath('*/*/');
    }
}